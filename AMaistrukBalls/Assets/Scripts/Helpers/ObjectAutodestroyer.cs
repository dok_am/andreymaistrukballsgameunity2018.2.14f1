﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ObjectAutodestroyer : MonoBehaviour {

    public enum AutodestroyerType : int {
        Timer,
        Coordinate
    }

    public enum ComparsionType : int {
        Lower,
        Bigger
    }

    public enum CoordinateType : int {
        x,
        y,
        z
    }

    public AutodestroyerType Type;

    [Header("Timer autodestroyer")]
    public float LiveDuration;

    [Header("Coordinate autodestroyer")]
    public CoordinateType Coordinate;
    public ComparsionType Condition;
    public float CoordinateConstant;

    private float _timer;

	private void Awake()
	{
        if (Type == AutodestroyerType.Timer) {
            Destroy(gameObject, LiveDuration);
        }
	}

    // Update is called once per frame
    void Update()
    {
        switch (Type) {
            case AutodestroyerType.Coordinate:
                CoordinateAutodestroy();
                break;

            default:
                break;
        }
    }

    void CoordinateAutodestroy() {
        float position = 0.0f;
        switch (Coordinate) {
            case CoordinateType.x:
                position = transform.position.x;
                break;

            case CoordinateType.y:
                position = transform.position.y;
                break;

            case CoordinateType.z:
                position = transform.position.z;
                break;
        }

        switch (Condition) {
            case ComparsionType.Bigger:
                if (position > CoordinateConstant) {
                    Destroy(gameObject);
                }
                break;

            case ComparsionType.Lower:
                if (position < CoordinateConstant) {
                    Destroy(gameObject);
                }
                break;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ObjectAutodestroyer))]
public class AutodestroyerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var destroyer = target as ObjectAutodestroyer;

        EditorGUILayout.LabelField("Autodestroyer type");
        destroyer.Type = (ObjectAutodestroyer.AutodestroyerType)GUILayout.SelectionGrid((int)destroyer.Type, new string[] { "Timer", "Coordinate" }, 2);

        switch (destroyer.Type)
        {
            case ObjectAutodestroyer.AutodestroyerType.Timer:
                EditorGUILayout.LabelField("Life duration");
                destroyer.LiveDuration = EditorGUILayout.FloatField(destroyer.LiveDuration);
                break;

            case ObjectAutodestroyer.AutodestroyerType.Coordinate:
                EditorGUILayout.LabelField("Coordinate");
                destroyer.Coordinate = (ObjectAutodestroyer.CoordinateType)EditorGUILayout.Popup((int)destroyer.Coordinate, new string[] { "x", "y", "x" });

                destroyer.Condition = (ObjectAutodestroyer.ComparsionType)EditorGUILayout.Popup((int)destroyer.Condition, new string[] { "<", ">" });

                destroyer.CoordinateConstant = EditorGUILayout.FloatField(destroyer.CoordinateConstant);
                break;
        }

    }
}
#endif
