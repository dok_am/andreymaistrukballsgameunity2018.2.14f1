﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//An event to pass BallController
[System.Serializable]
public class BallEvent: UnityEvent<BallController> {
    
}

public class BallController : MonoBehaviour {

    public float BallSpeed = 1.0f;
    public int BallScore;

    public float BallSize { get { return transform.localScale.x; }}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Translate(new Vector3(0.0f, BallSpeed * Time.fixedDeltaTime, 0.0f));
	}

}
