﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour {

    public BallEvent BallClickEvent;

    [SerializeField]
    private Camera _camera;

	private void Awake()
	{
        if (_camera == null) {
            _camera = Camera.main;
        }
	}

	void Update () {
        if (Input.GetMouseButtonDown(0)) {
            ProcessTouch();
        }
	}

    private void ProcessTouch() {
        RaycastHit hit;
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

        //max distance is 20 cuz it's ~ double as bubbles away from camera
        if (Physics.Raycast(ray, out hit, 20.0f, LayerMask.GetMask("Bubbles")))
        {
            Transform objectHit = hit.transform;

            BallController ball = objectHit.GetComponent<BallController>();

            if (ball != null) {
                BallClickEvent.Invoke(ball);
            }
        }

    }
}
