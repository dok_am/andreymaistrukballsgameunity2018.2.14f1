﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour {

    [Header("Objects")]
    public Camera MainCamera;
    public Transform BallsContainer;

    [Header("Prefabs")]
    public GameObject BallPrefab;

    [Header("Settings")]
    [SerializeField]
    private float _minimalBallSize;
    [SerializeField]
    private float _maximalBallSize;

    [Space]
    [SerializeField]
    private float _baseBallSpeed;
    [SerializeField]
    private float _ballSpeedFromSizeMultiplier = 1.0f;

    [Space]
    [SerializeField]
    private float _baseScore;
    [SerializeField]
    private float _ballScoreFromSizeMultiplier = 1.1f;


    //private variables
    private float _screenXMin;
    private float _screenXMax;
    private float _screenYMin;
    private float _screenYMax;

    private float _baseBallSpeedDefault;

    private void Awake()
    {
        if (MainCamera == null) {
            MainCamera = Camera.main;
        }

        Vector3 leftBottom = MainCamera.ViewportToWorldPoint(new Vector3(0, 0, -MainCamera.transform.position.z));
        _screenXMin = leftBottom.x;
        _screenYMin = leftBottom.y;
        Vector3 rightUp = MainCamera.ViewportToWorldPoint(new Vector3(1, 1, -MainCamera.transform.position.z));
        _screenXMax = rightUp.x;
        _screenYMax = rightUp.y;

        _baseBallSpeedDefault = _baseBallSpeed;
    }

    public void SpawnBall()
    {
        float randomSize = Random.Range(_minimalBallSize, _maximalBallSize);
        float sizePercent = Mathf.Clamp(randomSize / (_maximalBallSize - _minimalBallSize), 0, 1);
        float randomX = Random.Range(_screenXMin + randomSize / 2.0f, _screenXMax - randomSize / 2.0f);
        Vector3 randomPosition = new Vector3(randomX, _screenYMin - randomSize, 0.0f);

        BallController ball = Instantiate(BallPrefab, randomPosition, Quaternion.identity, BallsContainer).GetComponent<BallController>();
        ball.transform.localScale = new Vector3(randomSize, randomSize, randomSize);

        ball.BallSpeed = _baseBallSpeed + _baseBallSpeed * (1 - sizePercent) * _ballSpeedFromSizeMultiplier;
        ball.BallScore = Mathf.RoundToInt(_baseScore +  _baseScore * (1 - sizePercent) * _ballScoreFromSizeMultiplier);

        ObjectAutodestroyer autodestroyer = ball.GetComponent<ObjectAutodestroyer>();
        autodestroyer.CoordinateConstant = _screenYMax + randomSize * 2.0f;
        autodestroyer.Condition = ObjectAutodestroyer.ComparsionType.Bigger;
    }

    public void UpdateBaseBallSpeed(float valueToAdd) {
        _baseBallSpeed += valueToAdd;
    }

    public void ResetBaseBallSpeed() {
        _baseBallSpeed = _baseBallSpeedDefault;
    }
}
