﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Main UI")]
    public Text ScoreText;
    public Text TimeText;

    [Header("Start countdown")]
    public Text CountdownText;

    [Header("Game over")]
    public Animator GameOverUIObject;
    public Text GameOverFinalScoresText;

    private bool _isGameOverShown = false;

    #region Main UI

    public void SetScore(int score)
    {
        ScoreText.text = score.ToString();
    }

    public void SetTime(int remainingTime)
    {
        TimeText.text = remainingTime.ToString();
    }

    #endregion

    #region Countdown

    public void SetCountdownText(int remaining)
    {
        if (remaining == 0)
        {
            CountdownText.text = "GO!";
            return;
        }

        CountdownText.text = remaining.ToString();
    }

    public void TurnCountdownOnIfNeeded(bool on)
    {
        if (CountdownText.gameObject.activeSelf != on)
            CountdownText.gameObject.SetActive(on);
    }

    #endregion

    #region Game Over

    public void ShowGameOver(int finalScores) {
        if (!_isGameOverShown)
        {
            GameOverFinalScoresText.text = finalScores.ToString();
            GameOverUIObject.SetTrigger("show");
            _isGameOverShown = true;
        }
    }

    public void HideGameOver() {
        if (_isGameOverShown)
        {
            GameOverUIObject.SetTrigger("hide");
            _isGameOverShown = false;
        }
    }

    #endregion
}
