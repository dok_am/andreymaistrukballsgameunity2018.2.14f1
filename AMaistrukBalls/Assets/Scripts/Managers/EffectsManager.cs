﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Effect {

    public enum EffectType
    {
        BubblesBlowingEffect,
    }

    public EffectType Type;
    public GameObject Prefab;
}



public class EffectsManager : MonoBehaviour {

    public Effect[] Effects;

    public void ShowEffect(Effect.EffectType type, Vector3 position) {
        foreach (Effect eff in Effects) {
            if (eff.Type == type) {
                Instantiate(eff.Prefab, position, Quaternion.identity);
                return;
            }
        }

        Debug.Log("No such effect found");
    }

    public void ShowEffect(Effect.EffectType type, Vector3 position, float delay) {
        StartCoroutine(DelayedEffectRoutine(type, position, delay));
    }

    IEnumerator DelayedEffectRoutine(Effect.EffectType type, Vector3 position, float delay) {
        float timer = 0.0f;

        while (timer < delay) {
            timer += Time.deltaTime;
            yield return null;
        }

        ShowEffect(type, position);
    }
}
