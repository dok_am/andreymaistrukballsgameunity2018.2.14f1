﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public enum GameState {
        Prepare,
        Playing,
        GameOver
    }

    [Header("Managers")]
    public BallSpawner BallSpawner;
    public EffectsManager EffectsManager;
    public UIManager UIManager;

    [Header("Settings")]
    public int GameDuration = 60;
    public float SpeedPerSecondAccelerator = 0.1f;
    public float SpawnPeriodPerSecondAccelerator = 0.1f;
    public float SpawnMinimalPeriodValue = 0.1f;

    [Space]
    public float BallSpawnPeriod = 3.0f;

    [Header("Start countdown settings")]
    public int StartCountdownDuration = 3;

    [Header("Game over settings")]
    public bool WaitForAllBubbleToDie = true;
    public float GameOverHidingDelay = 1.0f;

    public GameState State { get; private set; }
    public int Score { get; private set; }

    //Private vars
    private float _startCountdownTimer = 0.0f;
    private float _mainGameTimer = 0.0f;
    private float _spawnTimer = 0.0f;
    private float _ballSpawPeriodDefault;

    private float _gameOverTimer = 0.0f;
    private bool _gameOverSkipped = false;

	private void Awake()
	{
        _ballSpawPeriodDefault = BallSpawnPeriod;
	}

	public void Update()
	{
        switch (State) {
            case GameState.Prepare:
                PrepareUpdate();
                break;

            case GameState.Playing:
                PlayingUpdate();
                break;

            case GameState.GameOver:
                GameOverUpdate();
                break;
        }
	}

    #region Game cycle

    private void PrepareUpdate() {
        //we doing +1 here cuz: 3,2,1 - GO! it's 1 second more than it supposed to think
        UIManager.TurnCountdownOnIfNeeded(true);
        UIManager.SetCountdownText((int)(StartCountdownDuration - _startCountdownTimer + 1));

        _startCountdownTimer += Time.deltaTime;

        if (_startCountdownTimer >= StartCountdownDuration + 1) {
            BeginGame();
        }
    }

    private void PlayingUpdate() {
        _mainGameTimer += Time.deltaTime;

        if (_mainGameTimer >= GameDuration)
        {
            FinishGame();
        }

        UIManager.SetTime((int)(GameDuration - _mainGameTimer));

        BallSpawner.UpdateBaseBallSpeed(SpeedPerSecondAccelerator * Time.deltaTime);

        BallSpawnPeriod = Mathf.Clamp(BallSpawnPeriod - SpawnPeriodPerSecondAccelerator * Time.deltaTime, SpawnMinimalPeriodValue, BallSpawnPeriod);

        SpawnUpdate();
    }

    private void SpawnUpdate() {
        _spawnTimer += Time.deltaTime;
        if (_spawnTimer >= BallSpawnPeriod)
        {
            BallSpawner.SpawnBall();
            _spawnTimer = 0.0f;
        }
    }

    private void GameOverUpdate() {
        //at first, wait till no bubbles left
        if (BallSpawner.BallsContainer.childCount != 0 && WaitForAllBubbleToDie)
            return;

        if (!_gameOverSkipped)
        {
            UIManager.ShowGameOver(Score);
            if (Input.GetMouseButtonDown(0)) {
                UIManager.HideGameOver();
                _gameOverSkipped = true;
            }

        } else {
            _gameOverTimer += Time.deltaTime;

            if (_gameOverTimer >= GameOverHidingDelay) {
                BeginPreparation();
            }

        }
    }

    #endregion

    #region Private

    private void BeginPreparation() {
        _startCountdownTimer = 0.0f;
        UIManager.SetScore(0);
        UIManager.SetTime(GameDuration);

        State = GameState.Prepare;
    }

    private void BeginGame() {
        UIManager.TurnCountdownOnIfNeeded(false);

        BallSpawner.ResetBaseBallSpeed();
        BallSpawnPeriod = _ballSpawPeriodDefault;

        Score = 0;
        _spawnTimer = BallSpawnPeriod;
        _mainGameTimer = 0.0f;

        State = GameState.Playing;
    }

    private void FinishGame() {
        _gameOverTimer = 0.0f;
        _gameOverSkipped = false;

        State = GameState.GameOver;
    }


    #endregion

    #region Events

    public void DidBallTouched (BallController ball) {
        Score += ball.BallScore;
        Debug.Log("Add scores: " + ball.BallScore);
        UIManager.SetScore(Score);
        EffectsManager.ShowEffect(Effect.EffectType.BubblesBlowingEffect, ball.transform.position);
        Destroy(ball.gameObject);
    }

    #endregion

}
